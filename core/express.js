var express = require('express');
	pkg = require('../package.json');

module.exports = function (app, config) {

// set up handlebars view engine
var handlebars = require('express-handlebars')
.create({ defaultLayout:'main' });

app.engine('handlebars', handlebars.engine);
app.set('view engine', 'handlebars');
//app.set('port', process.env.PORT || 3003);

app.use(express.static(config.global.root + '/public'));

var bodyParser = require('body-parser');

app.use(bodyParser.json());       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
	extended: true
}));

};