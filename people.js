people = {

	'1' : {
		'uid': '1',
		'name': 'Lydia Pang',
		'imgpath': '/images/one.jpg',
		'drink': 'Gin & Tonic', 
		'music': 'Rhye - Hunger',
	},

	'2' : {
		'uid': '2',
		'name': 'Roo Williams',
		'imgpath': '/images/two.jpg',
		'drink': 'Long Island Iced Tea',
		'music': 'Cursive - From the Hips' 
	},

	'3' : {
		'uid': '3',
		'name': 'Jeff Bowerman',
		'imgpath': '/images/three.jpg',
		'drink': 'JD & Coke',
		'music': 'Elton John - Candle in the Wind' 
	}

}

module.exports = people;