var path = require('path'),
    rootPath = path.normalize(__dirname + '/..'), //sets root path
    config,
    sharedConfig;

var sharedConfig = {
	root: rootPath,
	db : {
		path: {}
	}
};

config = {
	local: {
		mode:   'local',
		port:   3000,
		app: {
			name: 'BeaconPeople'
		},
		url:    '',
		global: sharedConfig
	},

	hosts: [
	{
		// domain: 'sentimeter.local',
		target: ['localhost:3000']
	}
	]

};


// Export config
module.exports = config;