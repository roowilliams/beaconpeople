var express = require('express');
var fs = require('fs');
var app = express();
var server = require('http').Server(app);

var env = process.env.NODE_ENV || 'local';
var config = require('./config/config')[env];
var port = process.env.PORT || 4040;

server.listen(port, function () {
	console.log('Server listening at port %d', port);
});

// express settings
require('./core/express')(app, config);

// Bootstrap routes
require('./core/routes')(app);

// app.listen(port, function() {
// 	console.log( 'Express started on http://localhost:' +
// 		app.get('port') + '; press Ctrl-C to terminate.' );
// });

var socketController = require('./controllers/SocketController');
socketController.setup(app, server, config);
var MsgController = require('./controllers/MsgController').init(app, server, socketController);

exports = module.exports = app;