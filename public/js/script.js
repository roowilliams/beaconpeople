var BeaconPeople = BeaconPeople || {};

BeaconPeople = {

	
	timer : false,

	visiblePeople : [],

	init : function() {

		this.SocketController.init();

		this.EventListeners.onPageStart();
	},


	// Model
	addPerson: function(person, cb) {
		
		
		var index = (_this.visiblePeople.push(person) - 1);
		console.log("index: " + index);
		_this.EventListeners.timeoutListenerUpdate(index, 5000);
		
		cb();

	},

	removePerson: function(index, cb) {
		console.log("removePerson index: " + index);
		if (index > -1) {
			_this.visiblePeople.splice(index, 1);
		}

		cb();
	},

	// controller
	SocketController : {

		socket : null,

		init: function () {

			console.log('script.js :: making connection');
			var connectionURL = window.location.hostname;
			this.socket = io.connect(connectionURL);

			this.listen();


		},

		emitMsg: function (msg, data) {

			this.socket.emit(msg, data);

		},



		listen: function() {
			_this.SocketController.socket.on('person', function(person) {
				
				var index = _this.Helpers.containsObject(person.uid, _this.visiblePeople);
				// if object isn't already in array
				if (index == -1) {
					_this.addPerson(person, _this.UI.updateDisplay);

				}
				else {
					_this.EventListeners.timeoutListenerUpdate(index, 5000);
				}
			});
		}
	},


	EventListeners : {
		onPageStart: function () {

			console.log('script.js :: event :: onPageStart');
			_this.UI.init();

			//will receive this event when a connection is made
			//_this.socket.on('data', _this.setupScreen);
		},

		timeoutListenerUpdate: function(index, time) {
			// create a timer variable based on index
			// if the same id comes through then overwrite variable
			if (_this.visiblePeople[index].timer) {
				clearTimeout(_this.visiblePeople[index].timer);
			}

			function timeoutFunction() {
				_this.removePerson(index, _this.UI.updateDisplay);
			};

			_this.visiblePeople[index].timer = setTimeout(timeoutFunction, time);

		}

	},


	// view
	UI : {

		init : function() {

			$('#one').hide();
			$('#two').hide();

		},

		updateDisplay: function() {
			console.log('updateDisplay');
			console.log('Array Length: ' +_this.visiblePeople.length);
			var html = '';
			for (i = 0; i < _this.visiblePeople.length; i++) {
				person = _this.visiblePeople[i];
				var uidClass = 'p' + person.uid;

				html = html + '<ul class="person ' +uidClass+'"><li><img src="' +person.imgpath+ '" /></li><li><h1>' +person.name+ '</h1></li><li>' +person.drink+ '</li><li>' +person.music+ '</li></ul>';
			}

			$('.people').html(html);
			console.log(html);
		},

	},

	Helpers : {

		containsObject : function(obj, list) {
			var i;
			for (i = 0; i < list.length; i++) {
				if (Object.is(list[i].uid, obj)) {
					return i;
				}
			}

			return -1;
		}
	}
}


var _this = BeaconPeople;

BeaconPeople.init();