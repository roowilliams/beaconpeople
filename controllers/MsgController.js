pkg = require('../package.json');

BeaconController = require('./BeaconController.js');
SocketController = null;

var beacons = require('../beacons');
var people = require('../people');

var MsgController = {

	init : function (app, server, socketController){
		BeaconController.init();
		console.log("BeaconController.init()");
		SocketController = socketController;
		return _self;
	},

	onNewSocketConnection : function(socket) {

		console.log('ui : new connection logged');
		_self.UI.setUpSocketEventListeners(socket);

	},

	onBeaconPresence : function(bleacon) {
		console.log(bleacon);
		
		if (bleacon.minor in beacons) {
			var id = beacons[bleacon.minor];
			var person = people[id];
			console.log(person);

			SocketController.emitMsg('person', person);
		}

	},

	UI : {

		setUpSocketEventListeners : function(socket) {

			socket.on('disconnect', function(socket) {
				console.log('appning.js: client disconnected.');
			});

		},
	}
}

_self = MsgController;

module.exports = _self;