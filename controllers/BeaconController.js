var Bleacon = require('bleacon');
MsgController = require('./MsgController.js');

var BeaconController = {

	init : function() {
		Bleacon.startScanning();

		_self.EventListeners.beaconListenerInit();
	},

	EventListeners : {

		beaconListenerInit: function() {
			console.log("BeaconController.js :: Listening for beacons...")
			
			Bleacon.on('discover', function(bleacon) {
					MsgController.onBeaconPresence(bleacon);
			});
		}
	}
}

var _self = BeaconController;

module.exports = BeaconController;