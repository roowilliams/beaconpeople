io = require('socket.io');

MsgController = require('./MsgController');

socketServer = null;

pkg = require('../package.json');


var SocketController = {

	setup : function (app, server, config) {
		//Start a Socket.IO listen

		socketServer = io.listen(server);

		//  ==================
		//  === ON CONNECT ===
		//  ==================

		socketServer.sockets.on('connection', function(socket) {

			MsgController.onNewSocketConnection(socket);

		});


			//MsgController.UI.newConnection(socket);


		//  ============================
		//  === SERVER ERROR LOGGING ===
		//  ============================

		socketServer.sockets.on('close', function(socket) {
			console.log('ui : socketServer has closed');
		});

	},

	emitMsg : function (msg, data) {

		socketServer.sockets.emit(msg, data);

	},

	emitMsgToClient : function (socket, msg, data) {

		socketId = socket.id;
		socketServer.sockets.emit(msg, data);

	}
}

var _self = SocketController;

module.exports = SocketController;